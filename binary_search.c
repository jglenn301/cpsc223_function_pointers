#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

/**
 * Searches the given array using the given comparison function, returning
 * whether it was found and either the location where the item was found or
 * the index where it would be inserted.
 *
 * This function is polymorphic -- it will work with any type.
 * Because it works with any type, it accepts the array as a void *
 * so we can pass any pointer to it whether it is a pointer to the
 * first element of an array of ints, the pointer to the first element
 * of an array of struct points, or whatever.  Like most functions that
 * work with arrays, it needs to know how many elements are in the array.
 * In order to do the pointer arithmetic, it needs to know how many bytes
 * each element occupies.
 *
 * @param array a pointer, non-NULL
 * @param key a pointer, non-NULL
 * @param index a pointer to an int to store the index at which the key
 * was found or should be inserted (or -1 if an error); non-NULL
 * @param item_count a non-negative integer
 * @param item_size a positive integer
 * @param compare a pointer to a comparison function
 * @return true if the key was found, false otherwise
 */
bool binary_search_i(const int array[], const int key, int *index, int item_count);
bool binary_search_s(const char * const array[], const char *key, int *index, int item_count);

int main(int argc, char **argv)
{
  // copy command line arguments into an array, converting to ints as we go
  // we con't check for numeric input and we don't check order
  int input_b[argc - 1];
  for (int i = 1; i < argc; i++)
    {
      input_b[i - 1] = atoi(argv[i]);
    }

  // min and max to search for are just before and just after inputs
  int min, max;
  if (argc == 1)
    {
      min = max = 0;
    }
  else
    {
      min = input_b[0] - 1;
      max = input_b[argc - 2] + 1;
    }

  // search for every int between min and max and output result
  for (int i = min; i <= max; i++)
    {
      int index;
      if (binary_search_i(input_b, i, &index, argc - 1))
	{
	  printf("%d: found at %d\n", i, index);
	}
      else if (index == -1)
	{
	  printf("%d: error searching\n", i);
	}
      else
	{
	  printf("%d: not found; insert at %d\n", i, index);
	}
    }

  // now search some strings
  const char *route[] = {"ACY", "BOS", "CMH", "DAY", "EUG", "FAT", "GRR"};
  char *keys[] = {"ABE", "BWI", "DEN", "FAR", "GYY"};
  for (int i = 0; i < sizeof(route) / sizeof(char *); i++)
    {
      printf("%s ", route[i]);
    }
  printf("\n");
  
  for (int i = 0; i < sizeof(keys) / sizeof(char *); i++)
    {
      int index;
      if (binary_search_s(route, keys[i], &index, sizeof(route) / sizeof(char *)))
	{
	  printf("%s: found at %d\n", keys[i], index);
	}
      else if (index == -1)
	{
	  printf("%s: error searching\n", keys[i]);
	}
      else
	{
	  printf("%s: not found; insert at %d\n", keys[i], index);
	}
    }
}

bool binary_search_i(const int a[], int key, int *index, int item_count)
{
  if (a == NULL || index == NULL || item_count < 0)
    {
      if (index != NULL)
	{
	  *index = -1;
	}
      return false;
    }

  int start = 0;
  int end = item_count - 1;
  while (start <= end)
    {
      int mid = (start + end) / 2;
      int result = key - a[mid];
      if (result == 0)
	{
	  *index = mid;
	  return true;
	}
      else if (result < 0)
	{
	  end = mid - 1;
	}
      else
	{
	  start = mid + 1;
	}
    }

  *index = start;
  return false;
}

bool binary_search_s(const char * const a[], const char *key, int *index, int item_count)
{
  if (a == NULL || key == NULL || index == NULL || item_count < 0)
    {
      if (index != NULL)
	{
	  *index = -1;
	}
      return false;
    }

  int start = 0;
  int end = item_count - 1;
  while (start <= end)
    {
      int mid = (start + end) / 2;
      int result = strcmp(key, a[mid]);
      if (result == 0)
	{
	  *index = mid;
	  return true;
	}
      else if (result < 0)
	{
	  end = mid - 1;
	}
      else
	{
	  start = mid + 1;
	}
    }

  *index = start;
  return false;
}
