#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <stdbool.h>

int multiply(int a, int b);
int add(int a, int b);
int min(int a, int b);
int max(int a, int b);

int accumulate(int *a, int n, int identity, int (*f)(int, int));

int main(int argc, char **argv)
{
  // copy command line arguments into an array
  int input[argc - 1];
  for (int i = 1; i < argc; i++)
    {
      input[i - 1] = atoi(argv[i]);
    }

  printf("sum = %d\n", accumulate(input, argc - 1, 0, add));
  printf("product = %d\n", accumulate(input, argc - 1, 1, multiply));
  printf("min = %d\n", accumulate(input, argc - 1, INT_MAX, min));
  printf("max = %d\n", accumulate(input, argc - 1, INT_MIN, max));

  return 0;
}

int multiply(int a, int b)
{
  return a * b;
}

int add(int a, int b)
{
  return a + b;
}

int min(int a, int b)
{
  return a < b ? a : b;
}

int max(int a, int b)
{
  return a > b ? a : b;
}

int accumulate(int *a, int n, int identity, int (*f)(int, int))
{
  int result = identity;
  for (int i = 0; i < n; i++)
    {
      result = f(result, a[i]);
    }
  return result;
}

