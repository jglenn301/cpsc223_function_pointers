CC = gcc
CFLAGS = -std=c99 -pedantic -Wall -g3

BinarySearch: binary_search.o
	${CC} ${CFLAGS} -o $@ $^

Accumulate: accumulate.o
	${CC} ${CFLAGS} -o $@ $^

clean:
	rm -r *.o BinarySearch Accumulate
